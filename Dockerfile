FROM nvidia/cuda:10.0-cudnn7-devel
ARG DEBIAN_FRONTEND=noninteractive

# install apt packages
RUN apt-get update && \
    apt-get install -yq python3-pip htop nano git wget libglib2.0-0 ffmpeg

# install python modules
ADD requirements.txt /
RUN pip3 install pip --upgrade && \
    pip install jupyter_contrib_nbextensions && \
    pip install -r requirements.txt

# setup juptyer
RUN jupyter nbextension enable --py --sys-prefix widgetsnbextension && \
  jupyter contrib nbextension install --user && \
  jupyter nbextension enable codefolding/main
RUN echo "c.NotebookApp.ip = '0.0.0.0'" >> /root/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.port = 8080" >> /root/.jupyter/jupyter_notebook_config.py

WORKDIR /root
EXPOSE 8080
CMD ["sh", "-c", "echo $JUPYTER_TOKEN && jupyter notebook --NotebookApp.token=$JUPYTER_TOKEN --no-browser --allow-root"]
