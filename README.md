# docker-image-dl-gpu

A Docker image for machine learning with a gpu.

Inspired by https://github.com/Cheukting/coursera-aml-docker/blob/master/requirements.txt


# How to use

```
docker build . -t docker.io/dice89/ubuntu-gpu-python-dl:cuda_spacy
docker push dice89/ubuntu-gpu-python-dl:cuda_spacy

```
